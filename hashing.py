#!/usr/bin/env python
#
# -*- coding: future_fstrings -*-
# Python 2.7:
# pip install future-fstrings

from __future__ import print_function, unicode_literals
import hashlib
import sys

def printHash(alg, msg):
    dig = getattr(hashlib, alg)(msg).hexdigest()
    print(f'{dig} {alg}({msg})')

msg = b'hello'
if 1 < len(sys.argv):
    msg = ' '.join(sys.argv[1:]).encode()

algs = 'md5', 'sha1', 'sha224', 'sha256', 'sha384', 'sha512'
for alg in algs:
    printHash(alg, msg)
