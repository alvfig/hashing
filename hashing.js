// hashing.js
const crypto = require('crypto');

function hashIt(alg, msg) {
    const hash = crypto.createHash(alg);
    hash.update(msg);
    return hash.digest('hex');
}

function main() {
    let msg = 'hello';
    if (2 < process.argv.length) {
        msg = process.argv.slice(2).join(' ');
    }
    const algs = ['md5', 'sha1', 'sha224', 'sha256', 'sha384', 'sha512'];
    algs.forEach(alg => console.log("%s %s('%s')", hashIt(alg, msg), alg, msg));
}

main()
