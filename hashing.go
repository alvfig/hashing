// hashing.go
// go run hashing.go [msg]...

package main

import (
	"fmt"
	"crypto/md5"
	"crypto/sha1"
	"crypto/sha256"
	"crypto/sha512"
	"os"
	"strings"
)

func main() {
	msg := []byte("hello")
	if 1 < len(os.Args) {
		msg = []byte(strings.Join(os.Args[1:], " "))
	}

	// md5
	fmt.Printf("%x md5(%q)\n", md5.Sum(msg), msg)

	// sha1
	fmt.Printf("%x sha1(%q)\n", sha1.Sum(msg), msg)

	// sha224
	fmt.Printf("%x sha224(%q)\n", sha256.Sum224(msg), msg)

	// sha256
	fmt.Printf("%x sha256(%q)\n", sha256.Sum256(msg), msg)

	// sha384
	fmt.Printf("%x sha384(%q)\n", sha512.Sum384(msg), msg)

	// sha512
	fmt.Printf("%x sha512(%q)\n", sha512.Sum512(msg), msg)
}
